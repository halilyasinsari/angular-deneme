# FROM stefanscherer/node-windows
# WORKDIR /app
# COPY package.json /app
# COPY . /app
# RUN npm install
# RUN npm run build --prod

# FROM stefanscherer/node-windows
# WORKDIR /app
# ENV PATH /app/node_modules/.bin:$PATH
# COPY package.json /app/package.json
# COPY . /app
# RUN npm --verbose install
# RUN npm install -g @angular/cli@7.3.9
# CMD ng serve --host 0.0.0.0

# FROM stefanscherer/node-windows
# ENV NODE_ENV=production
# WORKDIR /app
# COPY ["package.json", "package-lock.json*", "./"]
# RUN npm install --production
# COPY . .
# CMD [ "node", "server.js" ]

# Stage 1
FROM stefanscherer/node-windows as build-step
# RUN mkdir /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build --prod
# Stage 2
FROM e2eteam/nginx:1.14-alpine
COPY --from=build-step /app/dist/angular-deneme /usr/share/nginx/html