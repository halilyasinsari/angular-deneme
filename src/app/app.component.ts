import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {from} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-deneme';

  constructor (private http: HttpClient) {
    this.http.get('http://localhost:5000/Home/Trial',  {responseType: 'text'}).subscribe(data => {
      this.title = data;
      console.log(data);
    });
  }

}

//dotnet restore
//dotnet publish -o ./publish


//(docker file'ın olduğu yerde çalıştırılacak)
//docker build -t {image ismi} . 
//docker run --name {container ismi} -d -p {port numarası}:80 {çalıştırılacak olan image ismi}